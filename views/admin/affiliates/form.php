
	<div class="one_half" id="">
	
		<section class="title">
				<h4><?php echo lang('shop:common:affiliate'); ?></h4>
		</section>
		
			<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>

			<?php if(isset($id)): ?>
				<?php echo form_hidden('id', $id); ?>
			<?php endif; ?>

			<section class="item form_inputs">
				<div class="content">
					<fieldset>
						<ul>
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									<?php echo lang('shop:common:name'); ?>
									<span>*</span>
									<small>
										<?php echo lang('shop:options:name_description'); ?>
									</small>
								</label>
								<div class="input">
									<?php echo form_input('name', set_value('name', $name), 'id="name" placeholder="Affiliate name" '); ?>
								</div>
							</li>							
										
						</ul>

					</fieldset>
					
					<div class="buttons">
					
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save'))); ?>

						<a class="btn gray cancel" href="admin/shop/affiliates"><?php echo lang('shop:common:cancel'); ?></a>

					</div>


				<?php echo form_close(); ?>


			</div>
			
		</section>
	

	</div>

	<?php if(isset($id)): ?>

	<div class="one_half last" id="">
	
		<section class="title">
				<h4>Add User to group</h4>
		</section>
		
			<?php echo form_open('admin/shop/affiliates/add_user', 'class="crud"'); ?>


			<section class="item form_inputs">
				<div class="content">
					<fieldset>
						<ul>
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									<?php echo lang('shop:common:name'); ?>
									<span>*</span>
									<small>
										Search users by name
									</small>
								</label>
								<div class="input">
									<?php echo form_input('name', set_value('name', $name), 'id="name" placeholder="Affiliate name" '); ?>
								</div>
							</li>							
										
						</ul>

					</fieldset>
					
					<div class="buttons">
					
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save'))); ?>

						<a class="btn gray cancel" href="admin/shop/affiliates"><?php echo lang('shop:common:cancel'); ?></a>

					</div>


				<?php echo form_close(); ?>


			</div>
			
		</section>
	

	</div>
	<?php endif; ?>