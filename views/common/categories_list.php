<div>  

    {{shop:all_categories offset="{{offset}}" limit="{{limit}}" order-by="id" order-dir="asc" }}

        <ul>
            <li>
                 <a href="{{ url:site }}shop/categories/category/{{slug}}">{{name}}</a>
            </li>
        </ul>
        
    {{/shop:all_categories}}

        <div>
            <!-- Pagination -->
            <div class='pagination'> 
                {{ pagination:links }}
            </div>
        </div>

</div>

