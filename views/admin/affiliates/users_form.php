
	<div class="one_half" id="">
	
		<section class="title">
				<h4><?php echo lang('shop:common:affiliate'); ?></h4>
		</section>
		
			<?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>

			<?php if(isset($id)): ?>
				<?php echo form_hidden('id', $id); ?>
			<?php endif; ?>

			<section class="item form_inputs">
				<div class="content">
					<fieldset>
						<ul>
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									Display Name
									<small>
										The users Display name
									</small>
								</label>
								<div class="input">

									<?php echo $display_name; ?>

								</div>
							</li>							
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									First Name
									<small>
										The users first name
									</small>
								</label>
								<div class="input">

									<?php echo $first_name; ?>

								</div>
							</li>		
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									Surname
									<small>
										
									</small>
								</label>
								<div class="input">

									<?php echo $last_name; ?>
		
								</div>
							</li>		
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									Email
									<small>
										Email linked to account
									</small>
								</label>
								<div class="input">

									<?php echo $email; ?>
		
								</div>
							</li>																	
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									Affiliate Group
									<span>*</span>
									<small>
										Each user can ONLY be assigned to a single Affiliate group. If they require another group they will need another account.
									</small>
								</label>
								<div class="input">
									<?php echo $affiliate_select; ?>
								</div>
							</li>							
										
						</ul>

					</fieldset>
					
					<div class="buttons">
					
						<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save'))); ?>

						<a class="btn gray cancel" href="admin/shop/affiliates"><?php echo lang('shop:common:cancel'); ?></a>

					</div>


				<?php echo form_close(); ?>


			</div>
			
		</section>
	

	</div>



	<div class="one_half last" id="">
	
		<section class="title">
				<h4>Help</h4>
		</section>
		
		


			<section class="item form_inputs">
				<div class="content">
					<fieldset>
						<ul>
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									Topic name
									<span>*</span>
									<small>
										Topic description
									</small>
								</label>
								<div class="input">
									Some text to help about this
								</div>
							</li>							
							<li class="<?php echo alternator('even', ''); ?>">
								<label for="name">
									Topic name
									<span>*</span>
									<small>
										Topic description
									</small>
								</label>
								<div class="input">
									Some text to help about this
								</div>
							</li>			
						</ul>

					</fieldset>

			</div>
			
		</section>
	

	</div>
