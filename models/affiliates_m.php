<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
require_once(dirname(__FILE__) . '/' .'shop_model.php');
 
class Affiliates_m extends Shop_model {
	
	
	public $_table = 'shop_affiliates';
	private $affiliate_price_table = 'shop_affiliates_prices';
		
		
	public function __construct() 
	{
	
		parent::__construct();
		
		$this->_validation_rules = array(
		
			array(
				'field' => 'name',
				'label' => 'name',
				'rules' => 'required|trim'
			),
		);
		
	}
	

	/**
	 * Create a new affiliate
	 * @param  [type] $input [description]
	 * @return [type]        [description]
	 */
	public function create($input) 
	{
	
		$this->db->trans_start();
		
		$to_insert = array(
				'name' => $input['name'],				

		);
	
		$id = $this->insert($to_insert);
		
		$this->db->trans_complete();
		
		return ($this->db->trans_status() === FALSE) ? FALSE : $id;
	}	
	

	public function edit($input) 
	{
	
		$this->db->trans_start();
		
		$to_update = array(
				'name' => $input['name'],				
		);
	
		$id = $this->update($input['id'], $to_update);
		
		$this->db->trans_complete();
		
		return ($this->db->trans_status() === FALSE) ? FALSE : $id;
	}	


	public function delete($id)
	{
		if( parent::delete($id) )
		{
			// Now remove all affiliate links/prices
			$this->db->where('affiliate_id', $id)->delete( $this->affiliate_price_table );

			return TRUE;
		}

		return FALSE;
	}


	public function get_users_affiliate_group_id($profile_id)
	{
		//this will get many results, should only get 1
		$r = $this->db->where('user_id',$profile_id)->get('shop_users')->result();

		if(count($r) == 1)
		{
			return $r[0]->affiliate_id;
		}

		return 0;

	}

	/**
	 * We do not want to delete, just upate if exist, otherwise create
	 * 
	 * @param [type] $profile_id   [description]
	 * @param [type] $affiliate_id [description]
	 */
	public function set_users_affiliate_group_id($profile_id,$affiliate_id)
	{

		// First delete the current
		$this->db->where('user_id',$profile_id)->delete('shop_users');

		//then add the new
		$this->db->where('user_id',$profile_id)->update('shop_users', array('affiliate_id'=>$affiliate_id) );

		return TRUE;

	}	
	

	public function link($input)
	{
		

		//
		// First delete any records where the 
		// prod id, aff_id and min_qty are the same
		//
		$this->delete_price_row($input);


		// Then create the new record
		$to_insert = array(		
				'product_id' => $input['product_id'],
				'affiliate_id' => $input['affiliate_id'],
				'min_qty' => $input['min_qty'],
				'value' => $input['value'],
				'method' => $input['method'],		
		);
	
		$id = $this->db->insert( $this->affiliate_price_table ,$to_insert);
		
		return $id;

	}

	private function delete_price_row($input)
	{
		$this->db
			->where('product_id', $input['product_id'])
			->where('affiliate_id', $input['affiliate_id'])
			//->where('min_qty', $input['min_qty'] ) //only allow 1 record per affiliate+product for now, in future we can have multiple
			->delete( $this->affiliate_price_table );
	}
	

	public function build_dropdown($current_id = 0) 
	{

		$options =array();
		$options['field_property_id'] = 'affiliate_id';
		$options['current_id'] = $current_id;

		$affiliates = $this->db->order_by('name')->select('id, name')->get($this->_table)->result();
		return $this->_build_dropdown( $affiliates , $options );

	}	
}





























