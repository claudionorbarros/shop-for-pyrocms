<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');
/*
 * SHOP for PyroCMS
 * 
 * Copyright (c) 2013, Salvatore Bordonaro
 * All rights reserved.
 *
 * Author: Salvatore Bordonaro
 * Version: 1.0.0.051
 *
 *
 *
 * 
 * See Full license details on the License.txt file
 */
 
/**
 * SHOP			A full featured shopping cart system for PyroCMS
 *
 * @author		Salvatore Bordonaro
 * @version		1.0.0.051
 * @website		http://www.inspiredgroup.com.au/
 * @system		PyroCMS 2.1.x
 *
 */
class Affiliates_users extends Admin_Controller 
{
	protected $section = 'affiliates_users';
	private $data;	
	

	public function __construct() 
	{
		parent::__construct();

		$this->data = new StdClass();

		//check if has access
		role_or_die('shop', 'admin_affiliates');		
		
		// Load all the required classes
		$this->load->model('affiliates_m');
		//$this->load->model('affiliates_users_m');

		Events::trigger('evt_admin_load_assests');	

	}


	/**
	 * List all items
	 * @access public
	 */
	public function index($offset= 0) 
	{	

		$this->load->model('users/user_m');
		// Create pagination links
		$this->data->pagination = create_pagination('admin/users/index', $this->user_m->count_all());

		//Skip admin
		$skip_admin = '';

		// Using this data, get the relevant results
		$this->db->order_by('active', 'desc')
			->join('groups', 'groups.id = users.group_id')
			->where_not_in('groups.name', $skip_admin)
			->limit($this->data->pagination['limit'], $this->data->pagination['offset']);

		$this->data->users = $this->user_m->get_all();



		// Unset the layout if we have an ajax request
		if ($this->input->is_ajax_request())
		{
			$this->template->set_layout(false);
		}


		$this->template
				->title($this->module_details['name'])
				->build('admin/affiliates/users_list', $this->data);

		
	}

	/**
	 * 
	 * @param  [type] $id This is the Module USER id, not the id of the aff_user
	 * 
	 * @return [type]     [description]
	 */
	public function edit($id)
	{
		$this->load->model('users/user_m');



		// Using this data, get the relevant results
		$user = $this->db
				->select('profiles.first_name,profiles.last_name, profiles.display_name,profiles.user_id, users.email')
				->join('users', 'profiles.user_id = users.id')
				->where('user_id',$id)
				->get('profiles')->result();
				

		if(count($user) >=1)
		{
			$this->data = $user[0]; //its the first one


			if($input = $this->input->post())
			{

				$this->affiliates_m->set_users_affiliate_group_id($this->data->user_id, $input['affiliate_id'] );
			}

		}
		else
		{
			$this->session->set_flash_data('error','Unable to find profile');
			redirect('shop/affiliates');
		}


		// Now we need to see if this ser is assigned to a group
		$current_id = $this->affiliates_m->get_users_affiliate_group_id($this->data->user_id);

		//get the groups and by default set the current in select list
		$this->data->affiliate_select	= $this->affiliates_m->build_dropdown( $current_id );

		$this->template
				->title($this->module_details['name'])
				->build('admin/affiliates/users_form', $this->data);
	}



}