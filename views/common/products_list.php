<div id="MultipleItemListView">
    {{# This can be used to change the display format from grid|list #}}
    <div class="product-list">
        {{ if products }}    
            {{ products }}
                {{ if searchable != '1' }}
                    {{# Do not list searchable items in main list #}}
                {{ else }}
                <div itemscope itemtype="http://schema.org/Product" class="product-list-item">
                    <div itemprop="name" style="">
                        <a itemprop="url" href="{{ url:site }}shop/products/product/{{ slug }}">
                            <h4>{{ name }}</h4>
                        </a>
                    </div>
                    <div class="item-description"> {{ description }} </div>
                    <div class="item-price">Price ea: ${{ price }} </div>
                        <a itemprop="url" href="{{ url:site }}shop/products/product/{{ slug }}">
                            {{shop:images id="{{id}}" include_cover='YES' include_gallery='NO' }}
                                {{if local}}
                                    <img itemprop="image" src="{{ url:site }}files/thumb/{{file_id}}/200/200/" width="200" height="200" alt="{{alt}}" />
                                {{else}}
                                    <img itemprop="image" src="{{src}}" width="200" height="200" alt="{{alt}}" />
                                {{endif}}
                            {{/shop:images}}								
                        </a>
                        <br />
                        <form action="{{url:site}}shop/cart/add" name="" class="add-to-cart"method="post">
                            <input type="hidden" name="id" value="{{ id }}">
                                {{ if status != "in_stock" }}
                                    <a class="" href="{{ url:site }}shop/products/product/{{ slug }}">view</a>
                                {{ else }}
                                    {{ if category:user_data == 'prints' }}
                                        {{ shop:options id="{{id}}" }}	
                                            {{display}}								
                                        {{ /shop:options }}
                                        <div class="product-list-add-to-cart">
                                            <label class="qty-label">Quantity</label>
                                                <input class="qty-enter" name="quantity" id="quantity" data-max="0" data-min="" maxlength="5" title="Qty" value="1" />
                                                <input type="submit" value='add to cart' class="" />
                                        </div>
                                    {{ else }}
                                        <ul class="product-list-add-to-cart clearfix">
                                            <li><label class="qty-label">Quantity</label></li>
                                            <li><input class="qty-enter" name="quantity" id="quantity" data-max="0" data-min="" maxlength="5" title="Qty" value="1" /></li>
                                            <li><input type="submit" value='Add to cart' class="shopbutton" /></li>
                                        </ul>
                                    {{ endif }}
                                {{ endif }}
                        </form>	
                    </div>
                {{endif}}
            {{ /products }}
        {{ else }}
            <p><?php echo lang('shop:messages:product:no_products'); ?></p>
        {{ endif }}
    </div>
    {{ if pagination:links }} 
	<div class="pagination"> 
            {{ pagination:links }}
	</div>
    {{ endif}} 
</div>